import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-juros-simples',
  templateUrl: './juros-simples.component.html',
  styleUrls: ['./juros-simples.component.css']
})
export class JurosSimplesComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  capital: number;
  taxa: number;
  tempo: number;

  calcularJuros() {
    return this.capital * this.taxa * this.tempo;
  }
}
