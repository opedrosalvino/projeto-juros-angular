import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-juros-compostos',
  templateUrl: './juros-compostos.component.html',
  styleUrls: ['./juros-compostos.component.css']
})
export class JurosCompostosComponent implements OnInit {
  constructor() {}

  capital: number;
  taxa: number;
  tempo: number;

  ngOnInit() {}

  calcularJuros() {
    return this.capital * (1 + this.taxa) ** this.tempo;
  }

  exibirJurosCompostos() {
    let valores = [];
    for (let i = 1; i <= this.tempo; i++) {
      valores.push(this.capital * (1 + this.taxa) ** i);
    }
    return valores;
  }
}
